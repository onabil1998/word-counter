function countWords(){
    var text = document.getElementById("words_text");
    var startParagraphContainer = document.getElementById("countNum");
    let myArray;
    if (text.innerText.trim() === "") {
        startParagraphContainer.innerText = "0";
    }
    else {
        myArray = text.innerText.trim().split(/\s+/);
        startParagraphContainer.innerText = myArray.length;
        highlightLongestWord(myArray);
    }
}

function highlightLongestWord(myArray){
    var text = document.getElementById("words_text");

    let longestWord = myArray[0];
    for (let i = 1; i < myArray.length; i++) {
        if (myArray[i].length > longestWord.length) {
            longestWord = myArray[i];
        }
    }

    if (myArray[myArray.length-1] === longestWord) {
        text.innerHTML += "&nbsp"; 
    }
    
    var highlightedText = text.innerText.replace(new RegExp(longestWord, "g"), '<span style="background-color: blue;">$&</span>');
    text.innerHTML = highlightedText;
}
	
function assignButtonClickEvent() {
    const button = document.getElementById('start_button');
    button.addEventListener('click', countWords);
}

assignButtonClickEvent();